# My Stocks

Luciano Sousa Test for Liqid

Setup:
- Set the username and password on config/database.yml
- rails test to run tests
- rails s to run the app

ToDo:
- return the stock has generated since the start date to today
-- For a formula for the rate of return: https://en.wikipedia.org/wiki/Rate_of_return#Return 

- return the stock maximum drawdown has generated since the start date to today
-- For a definition and explanation about the maximum drawdown: https://ycharts.com/glossary/terms/max_drawdown

- StockTwits integration
