require 'test_helper'

class StockDataDecoratorTest < ActiveSupport::TestCase

  test 'it decorates a stock array data' do
    decorator = StockDataDecorator.new(['2017-12-01', 10.0, 12.0, 8.0, 11.5, 10000, 0.0, 1.0, 14.0, 12.5, 8.5, 12.0, 11000])
    assert_match decorator.date, '2017-12-01'
    assert_equal decorator.open_value, 10.0
    assert_equal decorator.high_value, 12.0
    assert_equal decorator.low_value, 8.0
    assert_equal decorator.close_value, 11.5
    assert_equal decorator.volume, 10000
    assert_equal decorator.ex_dividend, 0.0
    assert_equal decorator.split_ratio, 1.0
    assert_equal decorator.adj_open, 14.0
    assert_equal decorator.adj_high, 12.5
    assert_equal decorator.adj_low, 8.5
    assert_equal decorator.adj_close, 12.0
    assert_equal decorator.adj_volume, 11000
  end
end
 