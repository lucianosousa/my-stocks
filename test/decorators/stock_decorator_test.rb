require 'test_helper'

class StockDecoratorTest < ActiveSupport::TestCase

  test 'it decorates a stock data' do
    data = {
      'dataset' => {
        'dataset_code' => 200,
        'name' => 'Test Name',
        'start_date' => '2017-12-01',
        'end_date' => '2017-12-15',
        'column_names' => ['here', 'a', 'test'],
        'data' => [
          ["2017-12-01", 173.63, 174.17, 172.46, 173.87, 37054632.0, 0.0, 1.0, 173.63, 174.17, 172.46, 173.87, 37054632.0],
          ["2017-11-01", 172.4, 173.13, 171.65, 172.22, 20219307.0, 0.0, 1.0, 172.4, 173.13, 171.65, 172.22, 20219307.0]
        ]
      }
    }
    decorator = StockDecorator.new(data)
    assert_equal decorator.code, 200
    assert_match decorator.name, 'Test Name'
    assert_equal decorator.start_date, '2017-12-01'
    assert_equal decorator.end_date, '2017-12-15'
    assert_equal decorator.column_names, ['here', 'a', 'test']
    assert_match decorator.dataset_list.first.date, '2017-12-01'
    assert_match decorator.rate_of_return, "0.0096%"
  end

  test 'it decorates a nil stock data' do
    data = nil
    decorator = StockDecorator.new(data)
    assert_equal decorator.success?, false
  end
end
 