require 'test_helper'

class SearchControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get search_url
    assert_response :success
    assert_select 'p', 'Your best way to get infos about your stocks'
  end

  test "should retrieve correct search infos" do
    VCR.use_cassette("default search") do
      get search_url, params: { stock_name: "AAPL", start_date: "2017-12-01", end_date: "2017-12-17" }
      assert_response :success
      assert_select 'div', 'Stock code: AAPL'
      assert_select 'div', {count: 8}
    end
  end
end
