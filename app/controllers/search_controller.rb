class SearchController < ApplicationController
  def index
    if params[:stock_name]
      data = QuandlService.new(stock_name: params[:stock_name], start_date: params[:start_date], end_date: params[:end_date]).stock_prices
    end

    @data = StockDecorator.new(data)
  end
end
