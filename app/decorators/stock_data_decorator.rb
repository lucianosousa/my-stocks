class StockDataDecorator
  def initialize(stock_data)
    @stock_data = stock_data
  end

  def date
    @stock_data[0]
  end

  def open_value
    @stock_data[1]
  end

  def high_value
    @stock_data[2]
  end

  def low_value
    @stock_data[3]
  end

  def close_value
    @stock_data[4]
  end

  def volume
    @stock_data[5]
  end

  def ex_dividend
    @stock_data[6]
  end

  def split_ratio
    @stock_data[7]
  end

  def adj_open
    @stock_data[8]
  end

  def adj_high
    @stock_data[9]
  end

  def adj_low
    @stock_data[10]
  end

  def adj_close
    @stock_data[11]
  end

  def adj_volume
    @stock_data[12]
  end
end