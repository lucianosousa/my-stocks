class StockDecorator
  def initialize(stock)
    @stock = stock || NilStock.new
  end

  def success?
    @stock && @stock.success?
  end

  def code
    @stock['dataset']['dataset_code']
  end

  def name
    @stock['dataset']['name']
  end

  def start_date
    @stock['dataset']['start_date']
  end

  def end_date
    @stock['dataset']['end_date']
  end

  def column_names
    @stock['dataset']['column_names']
  end

  def rate_of_return
    return 0 if dataset_list.blank?
    initial_value = dataset_list.last.close_value
    current_value = dataset_list.first.close_value
    "#{((current_value - initial_value) / initial_value).round(4)}%"
  end

  def dataset_list
    @stock['dataset']['data'].map do |data|
      StockDataDecorator.new(data)
    end
  end
end

class NilStock
  def initialize
  end

  def success?
    false
  end
end