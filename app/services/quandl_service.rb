class QuandlService
  include HTTParty
  base_uri 'https://www.quandl.com/api/v3/datasets/WIKI'

  def initialize(stock_name:, start_date:, end_date:)
    @stock_name = stock_name
    @start_date = start_date
    @end_date = end_date
  end

  def stock_prices
    response = self.class.get("/#{@stock_name}.json?#{start_date}#{end_date}")
    return nil unless response && response.success?
    response
  end

  def start_date
    "start_date=#{@start_date}&" if @start_date
  end

  def end_date
    "end_date=#{@end_date}&" if @end_date
  end
end